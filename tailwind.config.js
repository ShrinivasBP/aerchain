const colors = require("tailwindcss/colors")
module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      ...colors,
      'orange': {
        100: '#F6D4B9',
        400: '#FF5E3A'
      },
      'white': {
        400: '#ffffff'
      },
      'custom': {
        100: "#417B73",
        200: "#E5F1F1"
      }
    },
    extend: {
      fontFamily: (theme) => ({
        hellix: ["Hellix", "sans-serif"],
      }),
      backgroundImage: {
        'header-background': "url('/img/background.svg')",
       },
      fontSize: {
        xxs: '11px',
        40: '40px',
      },
      padding: {
        22: '88px',
      },
      width: {
        30: '120px',
        '60vw': '60vw',
      },
      height: {
        '60vh': '60vh',
        '70vh': '70vh',
      }
    },
  },
  variants: {
    extend: {
      margin: ['responsive', 'hover', 'focus', 'group-hover'],
      backgroundAttachment: ['hover', 'focus'],
    },
  },
  plugins: [],
}
