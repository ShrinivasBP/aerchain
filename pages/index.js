import { DownOutlined } from '@ant-design/icons';
import { useState } from 'react';
import background from '../img/background.svg'

export default function Home() {
  const [currentNav, setCurrentNav] = useState('source')
  return (
    <>
      <div>
        <div style={{ background: 'url(https://assets-global.website-files.com/600ad8909e5574fbe8acbf6f/6176c3beac02e533bd067f32_Gradient%20-%20Header.svg)', backgroundSize: 'cover' }} >
          <div className="w-full b flex justify-start items-start flex-col" >
            <header className="flex w-full hover:white fixed bg-white-400 py-4 z-20">
              <div className="w-4/6 m-auto">
                <div className="flex justify-between item-center">
                  <ul className="w-2/3 list-di flex justify-between items-centre text-base mb-0 font-normal text-gray-900" >
                    <li className="flex justify-center items-center"><span className="text-orange-400 text-3xl mr-3 cursor-pointer">A</span><span className="text-xl font-semibold">Aerchain</span></li>
                    <li className="flex items-center cursor-pointer group hover:text-warmGray-500 transition-all ease-in-out duration-300">Home
                      <span className="group-hover:text-gray-500 mx-2 group-hover:mt-1 transition-all ease-in-out duration-300" style={{ fontSize: '8px' }} role="img" aria-label="down"><svg viewBox="64 64 896 896" focusable="false" data-icon="down" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M884 256h-75c-5.1 0-9.9 2.5-12.9 6.6L512 654.2 227.9 262.6c-3-4.1-7.8-6.6-12.9-6.6h-75c-6.5 0-10.3 7.4-6.5 12.7l352.6 486.1c12.8 17.6 39 17.6 51.7 0l352.6-486.1c3.9-5.3.1-12.7-6.4-12.7z"></path></svg></span>
                    </li>
                    <li className="flex items-center cursor-pointer group hover:text-warmGray-500 transition-all ease-in-out duration-300">About Us
                      <span className="group-hover:text-gray-500 mx-2 group-hover:mt-1 transition-all ease-in-out duration-300" style={{ fontSize: '8px' }} role="img" aria-label="down"><svg viewBox="64 64 896 896" focusable="false" data-icon="down" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M884 256h-75c-5.1 0-9.9 2.5-12.9 6.6L512 654.2 227.9 262.6c-3-4.1-7.8-6.6-12.9-6.6h-75c-6.5 0-10.3 7.4-6.5 12.7l352.6 486.1c12.8 17.6 39 17.6 51.7 0l352.6-486.1c3.9-5.3.1-12.7-6.4-12.7z"></path></svg></span>
                    </li>
                    <li className="flex items-center cursor-pointer group hover:text-warmGray-500 transition-all ease-in-out duration-300">Solution
                      <span className="group-hover:text-gray-500 mx-2 group-hover:mt-1 transition-all ease-in-out duration-300" style={{ fontSize: '8px' }} role="img" aria-label="down"><svg viewBox="64 64 896 896" focusable="false" data-icon="down" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M884 256h-75c-5.1 0-9.9 2.5-12.9 6.6L512 654.2 227.9 262.6c-3-4.1-7.8-6.6-12.9-6.6h-75c-6.5 0-10.3 7.4-6.5 12.7l352.6 486.1c12.8 17.6 39 17.6 51.7 0l352.6-486.1c3.9-5.3.1-12.7-6.4-12.7z"></path></svg></span>
                    </li>
                    <li className="flex items-center cursor-pointer hover:text-warmGray-500 transition-all ease-in-out duration-300">Pricing </li>
                    <li className="flex items-center cursor-pointer group hover:text-warmGray-500 transition-all ease-in-out duration-300">Blog
                      <span className="group-hover:text-gray-500 mx-2 group-hover:mt-1 transition-all ease-in-out duration-300" style={{ fontSize: '8px' }} role="img" aria-label="down"><svg viewBox="64 64 896 896" focusable="false" data-icon="down" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M884 256h-75c-5.1 0-9.9 2.5-12.9 6.6L512 654.2 227.9 262.6c-3-4.1-7.8-6.6-12.9-6.6h-75c-6.5 0-10.3 7.4-6.5 12.7l352.6 486.1c12.8 17.6 39 17.6 51.7 0l352.6-486.1c3.9-5.3.1-12.7-6.4-12.7z"></path></svg></span>
                    </li>
                  </ul>
                  <div className="w-1/5 flex justify-between items-center text-base font-normal text-black">
                    <h4 className="m-0"> Sign In </h4>
                    <button className="rounded-sm px-4 py-3 bg-coolGray-300 hover:bg-orange-400 hover:text-white-400 transition-all ease-in-out duration-200"> Request a demo </button>
                  </div>
                </div>
              </div>
            </header>
            <div className="w-4/6 flex justify-between m-auto">
              <div className="flex pt-36 pb-20 pr-10 w-1/2 justify-start flex-col">
                <p className="w-32 rounded-full flex text-xs bg-orange-100 text-orange-400 px-3 py-1 font-semibold mb-7">RATED #1 ON G2</p>
                <p className="text-6xl mb-1">People Strategy is</p>
                <p className="text-6xl mb-7 font-medium">Business Strategy</p>
                <p className="text-warmGray-600 text-xl font-light">Lattice is the complete people management platform that employees love to use</p>
                <p className="text-warmGray-600 text-xl mb-7 font-light"></p>
                <button className="rounded-sm p-2 bg-orange-400 h-14 w-4/12 text-white-400 text-xl"> See Aerchain </button>
              </div>
              <div className="flex w-1/2 justify-end items-center pt-14">
                <div className="flex">
                  <div className="flex relative mr-8">
                    <div className="w-96 h-96 rounded-2xl" style={{
                      background: "#FCCFAE",
                      background: "-webkit-linear-gradient(bottom, #FCCFAE, #F09B65)",
                      background: "-moz-linear-gradient(bottom, #FCCFAE, #F09B65)",
                      background: "linear-gradient(to top, #FCCFAE, #F09B65)"
                    }}>
                    </div>
                    <img className="absolute z-0 bottom-0 top-auto right-0 left-0 rounded-2xl" src="http://aerchain-prod.s3.us-west-2.amazonaws.com/website/Automation.png" width="390" height="420" />
                    <div className="absolute z-0 bottom-0 top-auto right-0 left-0 text-white-400 w-96 h-96 flex justify-end p-3 items-baseline flex-col text-sm rounded-xl" style={{
                      background: "#F38139",
                      background: "-webkit-gradient(linear,left bottom,left top,from(rgba(243, 129, 62, 0.8)),color-stop(50%,rgba(14,14,41,0)))"
                    }}>
                      {/* <p className="font-semibold m-0">CHRISTOPHER YEH</p>
                      <p className="m-0">CLIO</p> */}
                    </div>
                  </div>
                  {/* <div className="bg-white-400 flex justify-around w-56 rounded-md mt-2">
                    <span className="flex justify-around items-center">
                      <div className="w-9 h-6 flex justify-center items-center bg-orange-400 rounded-full ml-2">
                        <svg width="90%" height="70%" color="white" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M8.78601 5.74832C5.19589 5.74832 2.16052 7.20534 2.16052 9.00267C2.16052 10.3091 3.79988 11.4357 6.03838 11.9537M9.28605 12.2475C12.6425 12.1197 15.4115 10.7158 15.4115 9.00267C15.4115 7.79747 14.0038 6.74528 12.0374 6.18281" stroke="currentcolor" strokeLinecap="round" strokeLinejoin="round"></path>
                          <path d="M9.28601 5.79877L7.87215 4.38491V7.21262L9.28601 5.79877Z" stroke="currentcolor" strokeLinecap="round" strokeLinejoin="round"></path>
                          <path d="M8.95764 12.1992L10.3715 13.6131V10.7854L8.95764 12.1992Z" stroke="currentcolor" strokeLinecap="round" strokeLinejoin="round"></path>
                        </svg>
                      </div>
                      <p className="text-xxs px-2 py-2 m-0">With Lattice, Clio accelerated peer feedback by 10x</p>
                    </span>
                  </div> */}
                </div>
                {/* <div className="flex justify-between flex-col">
                  <div className="relative top-16 ml-8">
                    <div className="w-56 h-60 bg-violet-500 rounded-2xl" style={{
                      background: '#FCCFAE',
                      background: '-webkit-linear-gradient(bottom, #FCCFAE, #F09B65)',
                      background: '-moz-linear-gradient(bottom, #FCCFAE, #F09B65)',
                      background: 'linear-gradient(to top, #FCCFAE, #F09B65)'
                    }}>
                    </div>
                    <img className="absolute z-0 bottom-0 top-auto right-0 left-0" src="https://assets-global.website-files.com/600ad8909e5574fbe8acbf6f/6176c3beac02e55487067eed_Photo%20-%20Olivia%20Blair%20-%20Turo%20-%201.png" width="224px" height="250px" />
                    <div className="absolute z-0 bottom-0 top-auto right-0 left-0 text-white-400 w-56 h-60 flex justify-end p-3 items-baseline flex-col text-sm rounded-2xl" style={{
                      background: '#F38139',
                      background: '-webkit-gradient(linear,left bottom,left top,from(rgba(243, 129, 62, 0.8)),color-stop(50%,rgba(14,14,41,0)))'
                    }}>
                      <p className="font-semibold m-0">OLIVIA BAIR</p>
                      <p className="m-0">TURO</p>
                    </div>
                  </div>
                  <div className="bg-white-400 flex justify-around w-56 rounded-2xl mt-2 relative top-16 ml-8">
                    <span className="flex justify-around items-center">
                      <div className="w-9 h-6 flex justify-center items-center bg-orange-400 rounded-full ml-2">
                        <svg width="75%" height="75%" color="white" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M0.709473 5.08467C1.52593 2.50291 3.94954 0.630646 6.81238 0.630646C10.3454 0.630646 13.2095 3.48209 13.2095 6.99952C13.2095 10.5169 10.3454 13.3684 6.81238 13.3684C3.94954 13.3684 1.52593 11.4961 0.709473 8.91436" stroke="currentcolor" strokeLinecap="round" strokeLinejoin="round"></path>
                          <path d="M9.99936 5.55574C9.44797 4.35194 8.22859 3.51523 6.81292 3.51523C4.87976 3.51523 3.31262 5.07545 3.31262 7.00008C3.31262 8.92471 4.87976 10.4849 6.81292 10.4849C8.22859 10.4849 9.44797 9.64823 9.99936 8.44442" stroke="currentcolor" strokeLinecap="round" strokeLinejoin="round"></path>
                          <ellipse cx="6.8124" cy="6.99952" rx="0.965599" ry="0.961339" fill="currentcolor" stroke="currentcolor"></ellipse>
                        </svg>
                      </div>
                      <p className="text-xxs px-2 py-2 m-0">With Lattice Goals, Turo has the right tools to manage growth</p>
                    </span>
                  </div>
                </div> */}
              </div>
            </div>  ``
          </div>
        </div>
        <div className="w-4/6 m-auto flex justify-center items-center flex-col">
          <div className="w-full flex justify-between items-center py-8 border-b-2">
            <p className="text-sm font-light m-0"><span className="font-semibold">10+ </span>  ORGANIZATIONS TRUST AERCHAIN</p>
            <img src="https://aerchain-test.s3.us-west-2.amazonaws.com/ABInBev-logo.png" loading="lazy" alt="" className="clients-logo-svg" />
            <img width="100px" src="https://aerchain-devtest.s3.us-west-2.amazonaws.com/cars24-logo.jpeg" loading="lazy" alt="" className="clients-logo-svg" />
            <img src="https://aerchain-test.s3-us-west-2.amazonaws.com/Wework-logo.png" loading="lazy" alt="" className="clients-logo-svg" />
            <img  width="100px" src="https://aerchain-devtest.s3.us-west-2.amazonaws.com/UniversalSompo.png" loading="lazy" alt="" className="clients-logo-svg" />
            <img src="https://aerchain-prod.s3-us-west-2.amazonaws.com/Indiqube+logo.jpg" loading="lazy" alt="" className="clients-logo-svg" />
          </div>
          <div className="w-full flex justify-center items-center">
            <div className="w-full flex justify-between items-center flex-col py-32 px-40">
              <p className="text-xs font-semibold mb-4">LATTICE OVERVIEW</p>
              <p className="text-40 font-medium mb-6">It’s time to invest in your people</p>
              <p className="w-10/12 text-xl text-center text-warmGray-700">Lattice transforms your people strategy by connecting performance management, employee engagement, and career development in one unified solution.</p>
            </div>
          </div>
          <div className={`w-full flex justify-around items-center border-b mb-12 border-gray-200`}>
            <span className={`flex justify-around items-center p-4 cursor-pointer hover:border-b-2 hover:border-orange-400 group ${currentNav === 'source' ? 'border-b-2 border-orange-400' : ''}`} onClick={() => setCurrentNav('source')}>
              <div className={`w-10 h-10 flex justify-center items-center  rounded-full ml-2 transition-all ease-in-out duration-400 group-hover:text-white-400 group-hover:bg-orange-400 ${currentNav === 'source' ? 'bg-orange-400' : 'bg-coolGray-200'}`}>
                <svg width="75%" height="70%" className={` group-hover:text-white-400 ${currentNav === 'source' ? 'text-white-400' : 'text-orange-400'}`} viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.78601 5.74832C5.19589 5.74832 2.16052 7.20534 2.16052 9.00267C2.16052 10.3091 3.79988 11.4357 6.03838 11.9537M9.28605 12.2475C12.6425 12.1197 15.4115 10.7158 15.4115 9.00267C15.4115 7.79747 14.0038 6.74528 12.0374 6.18281" stroke="currentcolor" strokeLinecap="round" strokeLinejoin="round"></path>
                  <path d="M9.28601 5.79877L7.87215 4.38491V7.21262L9.28601 5.79877Z" stroke="currentcolor" strokeLinecap="round" strokeLinejoin="round"></path>
                  <path d="M8.95764 12.1992L10.3715 13.6131V10.7854L8.95764 12.1992Z" stroke="currentcolor" strokeLinecap="round" strokeLinejoin="round"></path>
                </svg>
              </div>
              <p className={`text-xl font-medium px-2 py-2 mb-0 group-hover:text-orange-400 ${currentNav === 'source' ? 'text-orange-400' : ''}`}>Source</p>
            </span>
            <span className={`flex justify-around items-center p-4 ${currentNav === 'procure' ? 'border-b-2 border-orange-400' : ''}`} onClick={() => setCurrentNav('procure')}>
              <div className={`w-10 h-10 flex justify-center items-center  rounded-full ml-2 ${currentNav === 'procure' ? 'bg-orange-400' : 'bg-coolGray-300'}`}>
                <svg width="75%" height="75%" color={currentNav === 'procure' ? "white" : "#FF5E3A"} viewBox="0 0 33 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M16.8314 10.6667L16.3192 11.0935L16.8314 11.708L17.3435 11.0935L16.8314 10.6667ZM11.498 21.3333L11.0266 21.8048L11.0326 21.8107L11.498 21.3333ZM10.8314 20.6667V20C10.4632 20 10.1647 20.2985 10.1647 20.6667H10.8314ZM10.1647 24.6667C10.1647 25.0349 10.4632 25.3333 10.8314 25.3333C11.1996 25.3333 11.498 25.0349 11.498 24.6667H10.1647ZM14.8314 21.3333C15.1996 21.3333 15.498 21.0349 15.498 20.6667C15.498 20.2985 15.1996 20 14.8314 20V21.3333ZM17.8873 9.39956L18.3994 9.82635L17.8873 9.39956ZM7.31283 17.1481L6.84143 17.6195L7.31283 17.1481ZM26.3201 17.1395L25.8511 16.6657L26.3201 17.1395ZM17.7623 25.6117L18.2313 26.0855L17.7623 25.6117ZM15.2729 9.83791L16.3192 11.0935L17.3435 10.2399L16.2972 8.98433L15.2729 9.83791ZM17.3435 11.0935L18.3994 9.82635L17.3752 8.97277L16.3192 10.2399L17.3435 11.0935ZM25.8511 16.6657L17.2933 25.1379L18.2313 26.0855L26.7891 17.6132L25.8511 16.6657ZM16.3588 25.1415L11.9634 20.856L11.0326 21.8107L15.428 26.0962L16.3588 25.1415ZM7.96945 16.8619L7.78424 16.6767L6.84143 17.6195L7.02664 17.8047L7.96945 16.8619ZM11.9695 20.8619L11.3028 20.1953L10.36 21.1381L11.0266 21.8047L11.9695 20.8619ZM10.1647 20.6667V24.6667H11.498V20.6667H10.1647ZM10.8314 21.3333H14.8314V20H10.8314V21.3333ZM18.3994 9.82635C20.3865 7.44183 24.0318 7.39035 26.0855 9.71781L27.0853 8.83565C24.4908 5.89526 19.8855 5.96029 17.3752 8.97277L18.3994 9.82635ZM7.56734 9.72909C9.62621 7.3957 13.2808 7.44731 15.2729 9.83791L16.2972 8.98433C13.7818 5.96578 9.16724 5.90061 6.56755 8.84693L7.56734 9.72909ZM6.56755 8.84693C4.32828 11.3848 4.44821 15.2263 6.84143 17.6195L7.78424 16.6767C5.88889 14.7814 5.79391 11.739 7.56734 9.72909L6.56755 8.84693ZM26.0855 9.71781C27.8609 11.73 27.7581 14.7778 25.8511 16.6657L26.7891 17.6132C29.1983 15.2282 29.3282 11.3777 27.0853 8.83565L26.0855 9.71781ZM17.2933 25.1379C17.0349 25.3937 16.6192 25.3953 16.3588 25.1415L15.428 26.0962C16.209 26.8577 17.4562 26.8529 18.2313 26.0855L17.2933 25.1379Z" fill="currentcolor"></path>
                </svg>
              </div>
              <p className={`text-xl font-medium px-2 py-2 mb-0 group-hover:text-orange-400 ${currentNav === 'procure' ? 'text-orange-400' : ''}`}>Procure</p>
            </span>
            <span className={`flex justify-around items-center p-4 ${currentNav === 'account' ? 'border-b-2 border-orange-400' : ''}`} onClick={() => setCurrentNav('account')}>
              <div className={`w-10 h-10 flex justify-center items-center  rounded-full ml-2 ${currentNav === 'account' ? 'bg-orange-400' : 'bg-coolGray-300'}`}>
                <svg width="75%" height="75%" color={currentNav === 'account' ? "white" : "#FF5E3A"} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fillRule="evenodd" clipRule="evenodd" d="M15.9999 17.3334C16.7363 17.3334 17.3333 16.7364 17.3333 16C17.3333 15.2636 16.7363 14.6667 15.9999 14.6667C15.2636 14.6667 14.6666 15.2636 14.6666 16C14.6666 16.7364 15.2636 17.3334 15.9999 17.3334ZM15.9999 18.6667C17.4727 18.6667 18.6666 17.4728 18.6666 16C18.6666 14.5273 17.4727 13.3334 15.9999 13.3334C14.5272 13.3334 13.3333 14.5273 13.3333 16C13.3333 17.4728 14.5272 18.6667 15.9999 18.6667Z" fill="currentcolor"></path>
                  <path fillRule="evenodd" clipRule="evenodd" d="M23.0877 17.3334C22.7651 17.3334 22.4907 17.5619 22.4003 17.8716C21.5914 20.6424 19.0321 22.6667 15.9999 22.6667C12.318 22.6667 9.33327 19.6819 9.33327 16C9.33327 12.3181 12.318 9.33335 15.9999 9.33335C19.0321 9.33335 21.5914 11.3576 22.4003 14.1284C22.4907 14.4381 22.7651 14.6667 23.0877 14.6667C23.5072 14.6667 23.8278 14.2885 23.7171 13.8839C22.7893 10.4925 19.6857 8.00002 15.9999 8.00002C11.5817 8.00002 7.99994 11.5817 7.99994 16C7.99994 20.4183 11.5817 24 15.9999 24C19.6857 24 22.7893 21.5075 23.7171 18.1161C23.8278 17.7116 23.5072 17.3334 23.0877 17.3334Z" fill="currentcolor"></path>
                  <path fillRule="evenodd" clipRule="evenodd" d="M27.9999 16C27.9999 22.6274 22.6274 28 15.9999 28C10.029 28 5.07656 23.639 4.15407 17.9284C4.09957 17.591 3.81453 17.3334 3.47276 17.3334C3.07629 17.3334 2.76502 17.6763 2.82602 18.0681C3.81976 24.4498 9.33954 29.3334 15.9999 29.3334C23.3637 29.3334 29.3333 23.3638 29.3333 16C29.3333 8.63622 23.3637 2.66669 15.9999 2.66669C9.33954 2.66669 3.81976 7.55026 2.82602 13.932C2.76502 14.3237 3.07629 14.6667 3.47276 14.6667C3.81453 14.6667 4.09957 14.409 4.15407 14.0716C5.07656 8.361 10.029 4.00002 15.9999 4.00002C22.6274 4.00002 27.9999 9.3726 27.9999 16Z" fill="currentcolor"></path>
                </svg>
              </div>
              <p className={`text-xl font-medium px-2 py-2 mb-0 group-hover:text-orange-400 ${currentNav === 'account' ? 'text-orange-400' : ''}`}>Account</p>
            </span>
            <span className={`flex justify-around items-center p-4 ${currentNav === 'analyse' ? 'border-b-2 border-orange-400' : ''}`} onClick={() => setCurrentNav('analyse')}>
              <div className={`w-10 h-10 flex justify-center items-center  rounded-full ml-2 ${currentNav === 'analyse' ? 'bg-orange-400' : 'bg-coolGray-300'}`}>
                <svg width="75%" height="75%" color={currentNav === 'analyse' ? "white" : "#FF5E3A"} viewBox="0 0 33 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M17.2793 10.9979V20.5922C17.2793 21.5727 18.284 22.217 19.0801 21.6445C21.6177 19.8196 23.2793 16.7776 23.2793 13.3334C23.2793 9.34494 21.0512 6.49585 17.8149 4.91315C17.477 4.74793 17.0815 4.74793 16.7437 4.91315C13.5074 6.49585 11.2793 9.34494 11.2793 13.3334C11.2793 16.1999 12.4303 18.7878 14.2793 20.6274" stroke="currentcolor" strokeWidth="1.36323" strokeLinecap="round" strokeLinejoin="round"></path>
                  <path d="M17.2797 19.3333C17.2797 25.1138 11.2799 25.4969 8.11289 24.5142C6.29775 23.9509 4.84743 22.7167 3.90653 21.2122C3.86723 21.1494 3.88272 21.0672 3.94137 21.0219V21.0219C6.31072 19.191 9.68124 19.4499 11.7432 21.6212L12.0338 21.9272" stroke="currentcolor" strokeWidth="1.36323" strokeLinecap="round" strokeLinejoin="round"></path>
                  <path d="M22.0774 21.9272L22.368 21.6212C24.43 19.4499 27.8005 19.191 30.1698 21.0219V21.0219C30.2285 21.0672 30.244 21.1494 30.2047 21.2122C29.2638 22.7167 27.8135 23.9509 25.9983 24.5142C24.2617 25.053 21.8077 25.1812 19.9453 24.234" stroke="currentcolor" strokeWidth="1.36323" strokeLinecap="round" strokeLinejoin="round"></path>
                </svg>
              </div>
              <p className={`text-xl font-medium px-2 py-2 mb-0 group-hover:text-orange-400 ${currentNav === 'analyse' ? 'text-orange-400' : ''}`}>Analyze</p>
            </span>
            <span className={`flex justify-around items-center p-4 ${currentNav === 'learn' ? 'border-b-2 border-orange-400' : ''}`} onClick={() => setCurrentNav('learn')}>
              <div className={`w-10 h-10 flex justify-center items-center  rounded-full ml-2 ${currentNav === 'learn' ? 'bg-orange-400' : 'bg-coolGray-300'}`}>
                <svg width="75%" height="75%" color={currentNav === 'learn' ? "white" : "#FF5E3A"} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect x="6.00065" y="7.33335" width="20" height="17.3333" rx="2" stroke="currentcolor" strokeWidth="1.33333"></rect>
                  <rect x="21.332" y="10.6667" width="1.33333" height="10.6667" rx="0.666667" fill="currentcolor"></rect>
                  <rect x="17.332" y="13.3333" width="1.33333" height="8" rx="0.666667" fill="currentcolor"></rect>
                  <rect x="13.332" y="10.6667" width="1.33333" height="10.6667" rx="0.666667" fill="currentcolor"></rect>
                  <rect x="9.33203" y="17.3333" width="1.33333" height="4" rx="0.666667" fill="currentcolor"></rect>
                </svg>
              </div>
              <p className={`text-xl font-medium px-2 py-2 mb-0 group-hover:text-orange-400 ${currentNav === 'learn' ? 'text-orange-400' : ''}`}>Learn</p>
            </span>
          </div>
          {currentNav === 'source' && (<div className={`w-full flex justify-around items-center mb-12`} style={{ backgroundImage: "url(https://assets-global.website-files.com/600ad8909e5574fbe8acbf6f/6176c3beac02e5297c067f3c_Gradient%20-%20Tabs.svg)", backgroundSize: "cover", backgroundRepeat: "no-repeat"}}>
            <div className="w-full rounded-3xl flex justify-between py-16 overflow-x-hidden">
              <div className="w-1/2 pl-20 pr-10">
                <h1 className="text-2xl font-medium ">We understand the chaos of Tail End Spend as well the complexity of Strategic Spend.</h1>
                <p className="text-xl text-gray-500 font-light">Sample text...</p>
                <div className="flex justify-between py-10 flex-wrap">
                  <div className="flex justify-start flex-col w-52">
                    <div className="flex justify-between items-center text-orange-400 text-lg">
                      <p className="m-0">Understand Unstructured Data</p>
                      <div className="w-5 h-5">
                        <svg width="100%" height="70%" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" clipRule="evenodd" d="M6.99931 6.00081L7 6.00012L5.58583 4.58587L5.58522 4.58658L1.34274 0.343669L0.399935 1.28648L4.38857 6.00081L0.400385 10.7141L1.34315 11.657L5.5851 7.41503L6.99931 6.00081Z" fill="currentcolor"></path>
                        </svg>
                      </div>
                    </div>
                    <p>Sample text.....</p>
                  </div>
                  <div className="flex justify-start flex-col w-52">
                    <div className="flex justify-between items-center text-orange-400 text-lg">
                      <p className="m-0">Category Specific Templates</p>
                      <div className="w-5 h-5">
                        <svg width="100%" height="70%" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" clipRule="evenodd" d="M6.99931 6.00081L7 6.00012L5.58583 4.58587L5.58522 4.58658L1.34274 0.343669L0.399935 1.28648L4.38857 6.00081L0.400385 10.7141L1.34315 11.657L5.5851 7.41503L6.99931 6.00081Z" fill="currentcolor"></path>
                        </svg>
                      </div>
                    </div>
                    <p>Sample text.....</p>
                  </div>
                  <div className="flex justify-start flex-col w-52">
                    <div className="flex justify-between items-center text-orange-400 text-lg">
                      <p className="m-0">Scenario Based Sourcing</p>
                      <div className="w-5 h-5">
                        <svg width="100%" height="70%" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" clipRule="evenodd" d="M6.99931 6.00081L7 6.00012L5.58583 4.58587L5.58522 4.58658L1.34274 0.343669L0.399935 1.28648L4.38857 6.00081L0.400385 10.7141L1.34315 11.657L5.5851 7.41503L6.99931 6.00081Z" fill="currentcolor"></path>
                        </svg>
                      </div>
                    </div>
                    <p>Sample text.....</p>
                  </div>
                  <div className="flex justify-start flex-col w-52">
                    <div className="flex justify-between items-center text-orange-400 text-lg">
                      <p className="m-0">Dynamic Negotiation Intelligence</p>
                      <div className="w-5 h-5">
                        <svg width="100%" height="70%" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" clipRule="evenodd" d="M6.99931 6.00081L7 6.00012L5.58583 4.58587L5.58522 4.58658L1.34274 0.343669L0.399935 1.28648L4.38857 6.00081L0.400385 10.7141L1.34315 11.657L5.5851 7.41503L6.99931 6.00081Z" fill="currentcolor"></path>
                        </svg>
                      </div>
                    </div>
                    <p>Sample text.....</p>
                  </div>
                  <div className="flex justify-start flex-col w-52">
                    <div className="flex justify-between items-center text-orange-400 text-lg">
                      <p className="m-0">Techno Commercial Evaluation</p>
                      <div className="w-5 h-5">
                        <svg width="100%" height="70%" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" clipRule="evenodd" d="M6.99931 6.00081L7 6.00012L5.58583 4.58587L5.58522 4.58658L1.34274 0.343669L0.399935 1.28648L4.38857 6.00081L0.400385 10.7141L1.34315 11.657L5.5851 7.41503L6.99931 6.00081Z" fill="currentcolor"></path>
                        </svg>
                      </div>
                    </div>
                    <p>Sample text.....</p>
                  </div>
                  <div className="flex justify-start flex-col w-52">
                    <div className="flex justify-between items-center text-orange-400 text-lg">
                      <p className="m-0">Dynamic Price Handling</p>
                      <div className="w-5 h-5">
                        <svg width="100%" height="70%" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" clipRule="evenodd" d="M6.99931 6.00081L7 6.00012L5.58583 4.58587L5.58522 4.58658L1.34274 0.343669L0.399935 1.28648L4.38857 6.00081L0.400385 10.7141L1.34315 11.657L5.5851 7.41503L6.99931 6.00081Z" fill="currentcolor"></path>
                        </svg>
                      </div>
                    </div>
                    <p>Sample text.....</p>
                  </div>
                </div>                
              </div>
              <div className="w-1/2 flex">
                <img className="rounded-l-3xl" width={'140%'} height="100%" alt="aerchain" src={"https://assets-global.website-files.com/600ad8909e5574fbe8acbf6f/6176c3beac02e57d5d067f23_Visual%20-%20Product%20-%20Performance.png"} />
              </div>
            </div>
          </div>)}
          <div className={`w-full flex justify-around items-center flex-col mb-12 p-40`}>
            <p className={`text-40 font-medium mb-0`} >Want to explore more?</p>
            <p className={`text-xl`}>Sign up for a Lattice account or ask to see our platform in action.</p>
            <button className={`text-xl bg-orange-400 text-white-400 rounded-md px-10 py-3`}>Request a demo</button>
          </div>
        </div>
        <footer className="w-4/6 m-auto">
          <div className="w-full text-gray-500">
            <div className="flex justify-between">
              <div className="space-y-4">
                <h5 className="text-xs tracking-widest font-semibold uppercase">Lattice</h5>
                <div className="flex flex-col space-y-4 text-base font-light text-gray-500">
                  <a href="/about" className="hover:text-black text-gray-500"><div>About us</div></a>
                  <a href="/careers" className="flex text-gray-500"
                  ><div>Careers</div>
                    <div className="tag-2 footer-link-tag">hiring</div></a
                  >
                  <a href="/pricing" className="hover:text-black text-gray-500"><div>Pricing</div></a>
                  <a href="/customers" className="hover:text-black text-gray-500"><div>Customers</div></a>
                  <a href="/blog" className="hover:text-black text-gray-500"><div>Blog</div></a>
                  <a href="/signin" className="hover:text-black text-gray-500"><div>Sign in</div></a>
                  <a href="/security" className="hover:text-black text-gray-500"><div>Security</div></a>
                </div>
              </div>
              <div className="space-y-4">
                <h5 className="text-xs tracking-widest font-semibold uppercase">our products</h5>
                <div className="flex flex-col space-y-4 text-base font-light text-gray-500">
                  <a href="/platform" className="hover:text-black text-gray-500"><div>Platform Overview</div></a>
                  <a href="/performance" className="hover:text-black text-gray-500"><div>Performance</div></a>
                  <a href="/engagement" className="hover:text-black text-gray-500"><div>Engagement</div></a>
                  <a href="/goals" className="hover:text-black text-gray-500"
                  ><div>Goals</div>
                    <div className="tag-2 footer-link-tag">New</div></a
                  >
                  <a href="/grow" className="hover:text-black text-gray-500"><div>Grow</div></a><a href="/analytics" className="hover:text-black text-gray-500"><div>Analytics</div></a>
                  <a href="/integrations" className="hover:text-black text-gray-500"><div>Integrations</div></a>
                </div>
              </div>
              <div className="space-y-4">
                <h5 className="text-xs tracking-widest font-semibold uppercase">resources</h5>
                <div className="flex flex-col space-y-4 text-base font-light text-gray-500">
                  <a href="/library" className="hover:text-black text-gray-500"><div>Library</div></a>
                  <a href="/type/articles" className="hover:text-black text-gray-500"><div>Articles</div></a>
                  <a href="/uniquely-led" className="hover:text-black text-gray-500"><div>Interview</div></a>
                  <a href="/webinars" className="hover:text-black text-gray-500"><div>Webinars</div></a>
                  <a href="/type/books" className="hover:text-black text-gray-500"><div>eBooks</div></a>
                  <a href="/all-hands-podcast" className="hover:text-black text-gray-500"><div>Podcasts</div></a>
                  <a href="/community/rfh" className="hover:text-black text-gray-500"><div>Community</div></a>
                  <a href="/templates" className="hover:text-black text-gray-500"><div>Templates</div></a>
                  <a href="/rfh-virtual-2021" className="hover:text-black text-gray-500"><div>RfH&nbsp;Virtual 2021</div></a>
                </div>
              </div>
              <div className="space-y-4">
                <h5 className="text-xs tracking-widest font-semibold uppercase">support</h5>
                <div className="flex flex-col space-y-4 text-base font-light text-gray-500">
                  <a href="https://help.latticehq.com/" target="_blank" className="hover:text-black text-gray-500" rel="noreferrer noopener"><div>Help&nbsp;Center</div></a>
                  <a href="/privacy/home" className="hover:text-black text-gray-500"><div>Privacy Statement</div></a>
                  <a href="https://status.lattice.com/" target="_blank" className="hover:text-black text-gray-500" rel="noreferrer noopener"><div>Status</div></a>
                  <a href="/privacy/request" className="hover:text-black text-gray-500"><div>Privacy&nbsp;Request</div></a>
                  <a href="/university" className="hover:text-black text-gray-500"><div>Lattice University</div></a>
                </div>
              </div>
              <div>
                <h5 className="text-xs tracking-widest font-semibold uppercase">contact us</h5>
                <div className="flex flex-col space-y-4 text-base font-light text-gray-500">
                  <a rel="noreferrer noopener" href="https://goo.gl/maps/7xag63HsHCbY1i91A" target="_blank" className="mb-7 hover:text-black text-gray-500">360 Spear St., Floor 4<br />San Francisco, CA 94105</a>
                </div>
                <a href="mailto:customercare@lattice.com" className="text-gray-500 hover:text-black">customercare(at)lattice.com</a>
              </div>
            </div>
            <div className="bg-gray-800 border my-10"></div>
            <div className="flex justify-between pt-5 pb-16 font-light text-xs">
              <div className="">© Copyright 2021. All Rights Reserved.</div>
              <div className="flex space-x-4">
                <a rel="noreferrer noopener" href="https://twitter.com/LatticeHQ" target="_blank" className="hover:text-blue-600"
                ><div className="footer-social-icon w-embed">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M24 12C24 18.6273 18.6273 24 12 24C5.37267 24 0 18.6273 0 12C0 5.37267 5.37267 0 12 0C18.6273 0 24 5.37267 24 12ZM18.6337 8.1263C18.1741 8.33018 17.6806 8.46772 17.162 8.53002C17.6911 8.21287 18.0981 7.71045 18.289 7.11176C17.7939 7.40545 17.2453 7.61903 16.662 7.73392C16.1944 7.23555 15.5286 6.92488 14.7915 6.92488C13.3757 6.92488 12.2285 8.07209 12.2285 9.48711C12.2285 9.68776 12.2511 9.88354 12.2948 10.0712C10.1654 9.96445 8.27711 8.94425 7.01339 7.39412C6.79333 7.77194 6.66631 8.21207 6.66631 8.68212C6.66631 9.57126 7.11856 10.3552 7.80625 10.8147C7.38636 10.8018 6.99154 10.6861 6.64608 10.4944C6.64527 10.5049 6.64527 10.5154 6.64527 10.5267C6.64527 11.7678 7.52875 12.8042 8.70105 13.0388C8.48585 13.0979 8.25931 13.1294 8.02631 13.1294C7.86046 13.1294 7.70026 13.1132 7.54412 13.0833C7.87016 14.1011 8.81594 14.8422 9.93727 14.8624C9.06027 15.5501 7.95592 15.9595 6.7553 15.9595C6.54819 15.9595 6.34431 15.9473 6.14367 15.9239C7.27794 16.6504 8.625 17.0751 10.0724 17.0751C14.7851 17.0751 17.3627 13.1707 17.3627 9.78403C17.3627 9.6732 17.3602 9.56235 17.3554 9.45233C17.8562 9.09149 18.2906 8.64004 18.6337 8.1263Z" fill="currentcolor"></path>
                    </svg></div
                  ></a>
                <a rel="noreferrer noopener" href="https://www.facebook.com/Lattice-1887608161466172" target="_blank" className="hover:text-blue-600"
                ><div className="footer-social-icon w-embed">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M24 12C24 18.6273 18.6273 24 12 24C5.37267 24 0 18.6273 0 12C0 5.37267 5.37267 0 12 0C18.6273 0 24 5.37267 24 12ZM10.5456 19H13.04V12.8048H15.12L15.4304 10.3904H13.0384V8.848C13.0384 8.152 13.232 7.672 14.2352 7.672H15.5152V5.5104C14.896 5.44504 14.2738 5.41353 13.6512 5.416C11.808 5.416 10.5456 6.5408 10.5456 8.608V10.3888H8.4656V12.8048H10.5456V19Z" fill="currentcolor"></path>
                    </svg></div
                  ></a>
                <a rel="noreferrer noopener" href="https://www.youtube.com/channel/UCcsGcdQ1NPkvl3RHaHkWK3A" target="_blank" className="hover:text-blue-600"
                ><div className="footer-social-icon w-embed">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24Z" fill="currentcolor"></path>
                      <path d="M19.0498 8.3623C19.0498 8.2998 19.0498 8.2373 18.9873 8.1748V8.1123C18.7998 7.5498 18.2998 7.1748 17.6748 7.1748H17.7998C17.7998 7.1748 15.3623 6.7998 12.0498 6.7998C8.7998 6.7998 6.2998 7.1748 6.2998 7.1748H6.4248C5.7998 7.1748 5.2998 7.5498 5.1123 8.1123V8.1748C5.1123 8.2373 5.1123 8.2998 5.0498 8.3623C4.9873 8.9873 4.7998 10.2998 4.7998 11.7998C4.7998 13.2998 4.9873 14.6123 5.0498 15.2373C5.0498 15.2998 5.0498 15.3623 5.1123 15.4248V15.4873C5.2998 16.0498 5.7998 16.4248 6.4248 16.4248H6.2998C6.2998 16.4248 8.7373 16.7998 12.0498 16.7998C15.2998 16.7998 17.7998 16.4248 17.7998 16.4248H17.6748C18.2998 16.4248 18.7998 16.0498 18.9873 15.4873V15.4248C18.9873 15.3623 18.9873 15.2998 19.0498 15.2373C19.1123 14.6123 19.2998 13.2998 19.2998 11.7998C19.2998 10.2998 19.1748 8.9873 19.0498 8.3623ZM14.4153 12.2888L11.2145 14.6043C11.1464 14.6724 11.0783 14.6724 11.0102 14.6724C10.9421 14.6724 10.874 14.6724 10.8059 14.6043C10.6696 14.5362 10.6015 14.4 10.6015 14.2638V9.63276C10.6015 9.49655 10.6696 9.36034 10.8059 9.29224C10.9421 9.22414 11.0783 9.22414 11.2145 9.29224L14.4153 11.6078C14.4834 11.6759 14.5515 11.8121 14.5515 11.9483C14.6196 12.0845 14.5515 12.2207 14.4153 12.2888Z" fill="white"></path>
                    </svg></div
                  ></a>
                <a rel="noreferrer noopener" href="https://www.linkedin.com/company/lattice-hq" target="_blank" className="hover:text-blue-600"
                ><div className="footer-social-icon w-embed">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M7.3 0.9C8.8 0.3 10.4 0 12 0C13.6 0 15.2 0.3 16.7 0.9C18.2 1.5 19.5 2.4 20.5 3.5C21.5 4.6 22.4 5.8 23.1 7.3C23.8 8.8 24 10.3 24 12C24 13.7 23.7 15.2 23.1 16.7C22.5 18.2 21.6 19.5 20.5 20.5C19.4 21.5 18.2 22.4 16.7 23.1C15.2 23.8 13.6 24 12 24C10.4 24 8.8 23.7 7.3 23.1C5.8 22.5 4.5 21.6 3.5 20.5C2.5 19.4 1.6 18.2 0.9 16.7C0.2 15.2 0 13.6 0 12C0 10.4 0.3 8.8 0.9 7.3C1.5 5.8 2.4 4.5 3.5 3.5C4.6 2.5 5.8 1.6 7.3 0.9ZM7.85709 8.53058C8.35913 8.53058 8.7775 8.36323 9.1122 8.1122C9.4469 7.86118 9.53058 7.44281 9.53058 7.02443C9.53058 6.60606 9.36323 6.27136 9.02853 6.02034C8.69383 5.76932 8.35913 5.60197 7.85709 5.60197C7.35504 5.60197 6.93667 5.76932 6.68564 6.02034C6.43462 6.27136 6.18359 6.60606 6.18359 7.02443C6.18359 7.44281 6.35094 7.77751 6.60197 8.1122C6.85299 8.4469 7.35504 8.53058 7.85709 8.53058ZM9.1122 16.898V9.7857H6.60197V16.898H9.1122ZM18.3164 16.898V13.1327C18.3164 11.9612 18.0654 11.0408 17.5633 10.3714C17.0613 9.70202 16.3082 9.36732 15.3878 9.36732C14.8857 9.36732 14.4674 9.53467 14.1327 9.7857C13.798 10.0367 13.4633 10.4551 13.3796 10.8735L13.2959 9.7857H10.7857L10.8694 11.4592V16.898H13.3796V13.1327C13.3796 12.6306 13.4633 12.2123 13.7143 11.8776C13.9653 11.5429 14.2164 11.4592 14.6347 11.4592C15.0531 11.4592 15.3878 11.6265 15.5551 11.8776C15.7225 12.1286 15.8898 12.547 15.8898 13.1327V16.898H18.3164Z" fill="currentcolor"></path>
                    </svg></div
                  ></a>
                <a rel="noreferrer noopener" href="https://www.instagram.com/uselatticehq/" target="_blank" className="hover:text-blue-600"
                ><div className="footer-social-icon w-embed">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" clipRule="evenodd" d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24ZM9.32041 5.53927C10.0138 5.50772 10.2353 5.5 12.0006 5.5H11.9986C13.7644 5.5 13.9852 5.50772 14.6785 5.53927C15.3705 5.57096 15.8431 5.68052 16.2575 5.84126C16.6854 6.00716 17.047 6.22925 17.4085 6.59083C17.7701 6.95213 17.9922 7.31479 18.1587 7.74232C18.3185 8.15563 18.4282 8.62798 18.4607 9.31999C18.4919 10.0134 18.5 10.2349 18.5 12.0003C18.5 13.7656 18.4919 13.9866 18.4607 14.68C18.4282 15.3717 18.3185 15.8442 18.1587 16.2577C17.9922 16.6851 17.7701 17.0477 17.4085 17.409C17.0474 17.7706 16.6853 17.9932 16.2579 18.1593C15.8443 18.32 15.3714 18.4296 14.6795 18.4613C13.9861 18.4928 13.7652 18.5005 11.9998 18.5005C10.2346 18.5005 10.0132 18.4928 9.31987 18.4613C8.62802 18.4296 8.15555 18.32 7.74198 18.1593C7.31474 17.9932 6.95209 17.7706 6.59093 17.409C6.2295 17.0477 6.00741 16.6851 5.84125 16.2575C5.68065 15.8442 5.57109 15.3719 5.53927 14.6799C5.50785 13.9865 5.5 13.7656 5.5 12.0003C5.5 10.2349 5.50812 10.0132 5.53914 9.31985C5.57028 8.62812 5.67997 8.15563 5.84112 7.74219C6.00768 7.31479 6.22977 6.95213 6.59133 6.59083C6.95263 6.22938 7.31528 6.00729 7.74279 5.84126C8.15609 5.68052 8.62843 5.57096 9.32041 5.53927ZM11.784 6.67134C11.6525 6.67128 11.5307 6.67122 11.4175 6.6714V6.66978C10.206 6.67113 9.97367 6.67926 9.37404 6.70634C8.74028 6.73546 8.39619 6.84109 8.16706 6.93046C7.86372 7.04855 7.64705 7.18939 7.41955 7.4169C7.19205 7.64441 7.05094 7.86109 6.93313 8.16443C6.84416 8.39357 6.73826 8.73754 6.70928 9.37131C6.67814 10.0566 6.67191 10.2613 6.67191 11.9971C6.67191 13.733 6.67814 13.9388 6.70928 14.6241C6.73813 15.2578 6.84416 15.6018 6.93313 15.8307C7.05121 16.1342 7.19205 16.3503 7.41955 16.5778C7.64705 16.8053 7.86372 16.9462 8.16706 17.064C8.39632 17.1529 8.74028 17.2588 9.37404 17.2881C10.0593 17.3192 10.265 17.326 12.0006 17.326C13.7361 17.326 13.942 17.3192 14.6272 17.2881C15.2609 17.2591 15.6052 17.1535 15.834 17.0641C16.1375 16.9463 16.3535 16.8055 16.581 16.5779C16.8085 16.3506 16.9496 16.1346 17.0674 15.8312C17.1564 15.6024 17.2623 15.2584 17.2913 14.6246C17.3224 13.9394 17.3292 13.7335 17.3292 11.9988C17.3292 10.264 17.3224 10.0582 17.2913 9.37294C17.2624 8.73916 17.1564 8.39519 17.0674 8.16633C16.9493 7.86298 16.8085 7.64631 16.581 7.4188C16.3536 7.19129 16.1374 7.05045 15.834 6.93263C15.6049 6.84366 15.2609 6.73776 14.6272 6.70878C13.9418 6.67763 13.7361 6.6714 12.0006 6.6714C11.9256 6.6714 11.8534 6.67137 11.784 6.67134ZM14.6903 8.53048C14.6903 8.0997 15.0397 7.75072 15.4703 7.75072V7.75045C15.9009 7.75045 16.2503 8.09983 16.2503 8.53048C16.2503 8.96112 15.9009 9.31051 15.4703 9.31051C15.0397 9.31051 14.6903 8.96112 14.6903 8.53048ZM8.66255 12.0003C8.66255 10.1568 10.1571 8.66218 12.0005 8.66211C13.8439 8.66211 15.3381 10.1568 15.3381 12.0003C15.3381 13.8438 13.8441 15.3377 12.0006 15.3377C10.1572 15.3377 8.66255 13.8438 8.66255 12.0003ZM14.1673 12.0003C14.1673 10.8035 13.1972 9.83351 12.0006 9.83351C10.8039 9.83351 9.83392 10.8035 9.83392 12.0003C9.83392 13.1969 10.8039 14.167 12.0006 14.167C13.1972 14.167 14.1673 13.1969 14.1673 12.0003Z" fill="currentcolor"></path>
                    </svg></div
                  ></a>
                <a rel="noreferrer noopener" href="https://medium.com/latticehq" target="_blank" className="hover:text-blue-600"
                ><div className="footer-social-icon w-embed">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" clipRule="evenodd" d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24ZM8.35866 17.7907C11.318 17.7907 13.7171 15.3752 13.7171 12.3954C13.7171 9.41571 11.3182 7 8.35866 7C5.39915 7 3 9.41571 3 12.3954C3 15.3752 5.39933 17.7907 8.35866 17.7907ZM19.5956 12.3954C19.5956 15.2002 18.396 17.4748 16.9162 17.4748C15.4365 17.4748 14.2369 15.2002 14.2369 12.3954C14.2369 9.59066 15.4363 7.31605 16.9161 7.31605C18.3958 7.31605 19.5954 9.58994 19.5954 12.3954H19.5956ZM21.0577 16.9458C21.5782 16.9458 22 14.9078 22 12.3954C22 9.88232 21.578 7.84511 21.0577 7.84511C20.5374 7.84511 20.1155 9.88251 20.1155 12.3954C20.1155 14.9084 20.5372 16.9458 21.0577 16.9458Z" fill="currentcolor"></path>
                    </svg></div
                  ></a>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </>
  )
}
